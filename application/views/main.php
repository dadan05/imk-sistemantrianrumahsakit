<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Selamat Datang</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/') ?>style.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Dosis&display=swap" rel="stylesheet">
    </head>
    <body>
        <div class="header" style="padding-top: 40px;">
                    <h1 style="text-align: center;"><b>Selamat datang di situs nomor antrian rumah sakit cerdas</b></h1>
                    <p style="text-align: center;">Web yang melayani pasien dengan pemberian nomor antrian yang cerdas,ramah dan efektif</p>
                        <ul class="nav justify-content-center" style="padding-top: 27px;">
                            <li class="nav-item">
                                <a class="nav-link active" href="<?php echo base_url('Welcome')?>">Pendaftaran</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" style="color: black;" href="<?php echo base_url('Welcome/jadwal')?>">Cek Jadwal</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" style="color: black;" href="#">Tata Cara</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" style="color: black;" href="<?php echo base_url('Welcome/about')?>">Tentang Kami</a>
                            </li>
                        </ul>
        </div>
        <div class="container" style="margin-top: 100px;">
                    <div class="judul" style="text-align: center;">
                       <h6 style="margin-bottom: 20px;">Alur Pendaftaran</h6>
                        <img src="<?php echo base_url('asset/') ?>img/1.login.png" alt="log" style="width: 250px; height: auto;">
                    </div> 
                </div>
                <div class="kotak_login" style="">
                <p class="tulisan_login"><b>Silahkan login untuk mendaftar nomor antrian</b></p>
                <form action="pendaftaran.html" method="POST" onSubmit="validasi()" >
                    <tr>
                        <td><label>No.Rujukan :</label></td>
                        <input type="text" name="norujukan" class="form_login" placeholder="Isi nomor Rujukan anda" name="id">
                    </tr>
                    <tr>
                        <td><label>Nama :</label></td>
                        <input type="text" name="nama" class="form_login" placeholder="Isi Nama Lengkap anda">
                    </tr>
                    </br>
                    </br>
                    <a class="selesai" href="<?php echo base_url('Welcome/login')?>">Login</a>
                    </form>
                </div>
                </div>
    </br>
        <section class="footer">
            <div class="contact">
                <p>Hubungi Kami</p>
                    <ol>
                        <ul><img src="<?php echo base_url('asset/') ?>img/iconfinder_94_171453.png" style="width: 15px; height: 15px; color: white;" alt="">Jl.Rumah sakit no.86</ul>
                        <ul><img src="<?php echo base_url('asset/') ?>img/iconfinder_phone_326545.png" style="width: 15px; height: 15px; color:white;" alt="">023-3444545</ul>
                        <ul><img src="<?php echo base_url('asset/') ?>img/iconfinder_aiga_mail_134146.png" style="width: 15px; height: 15px;" alt="">rumahsakitcerdas@yahoo.com</ul>
                        <ul><img src="<?php echo base_url('asset/') ?>img/iconfinder_globe_172473.png" style="width: 15px; height: 15px; color:white;" alt="">www.rumahsakitcerdas.com</ul>
                    </ol>
            </div>
        </section>



        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>