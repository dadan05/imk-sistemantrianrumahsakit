<!DOCTYPE html>
<html>
    <head>
        <title>Pemilihan Jadwal</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/') ?>style.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Dosis&display=swap" rel="stylesheet">
    </head>
    <body>
        <div class="header">
            <h1 style="text-align: center;"><b>Selamat datang di situs nomor antrian rumah sakit cerdas</b></h1>
            <p style="text-align: center;">Web yang melayani pasien dengan pemberian nomor antrian yang cerdas,ramah dan efektif</p>
            <hr style="color:black; margin:0px auto;">
            <nav>
                <ul style="margin:0px;">
                    <a href="index.html">Beranda</a>
                    <a href="pendaftaran.html">Pendaftaran</a>
                    <a href="tatacara.html">Tata Cara</a>
                    <a href="peraturan.html">Peraturan</a>
                    <a href="about.html">Tentang Kami</a>
                </ul>
            </nav>
        </div>
        
        <div class="tutorial">
            <p><b>Tata Cara mendaftar Nomor Antrian di Rumah Sakit</b></p>
            <img src="<?php echo base_url('asset/') ?>img/progress3.png" alt="" style="width:300px; height:200px;">
        </div>
        <p class="text-center"><b>Jadwal Praktek Poli Saraf</b></p>
        <p class="text-center"><b>Dr.Endang.ST,M.Kom</b></p>
        <div class="row" id="Jadwal1">
            <div class="col-md-6">
                <p><b>Senin,01 Januari 2020</b></p>
                <table class="table1">
                    <tr>
                        <th>Pilih</th>
                        <th>Kategori Pasien</th>
                        <th>Jadwal</th>
                        <th>Sisa Kuota</th>
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="ss0809"/></td> <!--Format Value = hari-poli-tgl -->
                        <td>Prioritas</td>
                        <td>Senin,08:00-09:00</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="ss0910"/></td>
                        <td>Prioritas</td>
                        <td>Senin,09:00-10:00</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="ss1011"/></td>
                        <td>Biasa</td>
                        <td>Senin,10:00-11:00</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="ss1112"/></td>
                        <td>Biasa</td>
                        <td>Senin,11:00-12:00</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="ss1314"/></td>
                        <td>Biasa</td>
                        <td>Senin,13:00-14:00</td>
                        <td></td>
                    </tr>
                </table>
            </div>
            <div class="col-md-6">
                <p><b>Selasa,02 Januari 2020</b></p>
                <table class="table1">
                    <tr>
                        <th>Pilih</th>
                        <th>Kategori Pasien</th>
                        <th>Jadwal</th>
                        <th>Sisa Kuota</th>
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="sas0809"/></td>
                        <td>Prioritas</td>
                        <td>Selasa,08:00-09:00</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="sas0910"/></td>
                        <td>Prioritas</td>
                        <td>Selasa,09:00-10:00</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="sas1011"/></td>
                        <td>Biasa</td>
                        <td>Selasa,10:00-11:00</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="sas1112"/></td>
                        <td>Biasa</td>
                        <td>Selasa,11:00-12:00</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="sas1314"/></td>
                        <td>Biasa</td>
                        <td>Selasa,13:00-14:00</td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row" id="Jadwal2">
            <div class="col-md-6">
                <p><b>Rabu,03 Januari 2020</b></p>
                <table class="table1">
                    <tr>
                        <th>Pilih</th>
                        <th>Kategori Pasien</th>
                        <th>Jadwal</th>
                        <th>Sisa Kuota</th>
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="rs0809"/></td>
                        <td>Prioritas</td>
                        <td>Rabu,08:00-09:00</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="rs0910"/></td>
                        <td>Prioritas</td>
                        <td>Rabu,09:00-10:00</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="rs1011"/></td>
                        <td>Biasa</td>
                        <td>Rabu,10:00-11:00</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="rs1112"/></td>
                        <td>Biasa</td>
                        <td>Rabu,11:00-12:00</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="rs1314"/></td>
                        <td>Biasa</td>
                        <td>Rabu,13:00-14:00</td>
                        <td></td>
                    </tr>
                </table>
            </div>
            <div class="col-md-6">
                <p><b>Kamis,04 Januari 2020</b></p>
                <table class="table1">
                    <tr>
                        <th>Pilih</th>
                        <th>Kategori Pasien</th>
                        <th>Jadwal</th>
                        <th>Sisa Kuota</th>
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="ks0809"/></td>
                        <td>Prioritas</td>
                        <td>Kamis,08:00-09:00</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="ks0910"/></td>
                        <td>Prioritas</td>
                        <td>Kamis,09:00-10:00</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="ks1011"/></td>
                        <td>Biasa</td>
                        <td>Kamis,10:00-11:00</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="ks1112"/></td>
                        <td>Biasa</td>
                        <td>Kamis,11:00-12:00</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="ks1314"/></td>
                        <td>Biasa</td>
                        <td>Kamis,13:00-14:00</td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
        <section class="footer">
            <div class="contact">
                <p>Hubungi Kami</p>
                <ol>
                    <ul><img src="<?php echo base_url('asset/') ?>img/iconfinder_94_171453.png" style="width: 15px; height: 15px; color: white;" alt="">Jl.Rumah sakit no.86</ul>
                    <ul><img src="<?php echo base_url('asset/') ?>img/iconfinder_phone_326545.png" style="width: 15px; height: 15px; color:white;" alt="">023-3444545</ul>
                    <ul><img src="<?php echo base_url('asset/') ?>img/iconfinder_aiga_mail_134146.png" style="width: 15px; height: 15px;" alt="">rumahsakitcerdas@yahoo.com</ul>
                    <ul><img src="<?php echo base_url('asset/') ?>img/iconfinder_globe_172473.png" style="width: 15px; height: 15px; color:white;" alt="">www.rumahsakitcerdas.com</ul>
                </ol>
            </div>
            <hr color="#DCDCDC;">
            <p>Copyright &copy;2019 Design By Nogi</p>
        </section>

        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>
