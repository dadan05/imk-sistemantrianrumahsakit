<!DOCTYPE html>
<html>
<head>
	<title>Pendaftaran</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/') ?>style.css">
	<link href="https://fonts.googleapis.com/css?family=Dosis&display=swap" rel="stylesheet">
</head>
<body>
	<div class="header">
		<h1 style="text-align: center;"><b>Selamat datang di situs nomor antrian rumah sakit cerdas</b></h1>
		<p style="text-align: center;">Web yang melayani pasien dengan pemberian nomor antrian yang cerdas,ramah dan efektif</p>
		<hr style="color:black; margin:0px auto;">
		<nav>
			<ul style="margin:0px;">
				<a href="index.html">Beranda</a>
				<a href="pendaftaran.html">Pendaftaran</a>
				<a href="tatacara.html">Tata Cara</a>
				<a href="peraturan.html">Peraturan</a>
				<a href="about.html">Tentang Kami</a>
			</ul>
		</nav>
	</div>
	<div class="tutorial">
		<p>Tata Cara mendaftar Nomor Antrian di Rumah Sakit</p>
		<img src="<?php echo base_url('asset/') ?>img/progress1.png" alt="" style="width:300px; height:200px;">
	</div>
		<div class="kotak_login">
		<p class="tulisan_login"><b>Silahkan login untuk mendaftar nomor antrian</b></p>
		<form action="pendaftaran.html" method="POST" onSubmit="validasi()" >
			<tr>
				<td><label>No.Rujukan :</label></td>
				<input type="text" name="norujukan" class="form_login" placeholder="Isi nomor Rujukan anda">
			</tr>
			<tr>
				<td><label>Nama :</label></td>
				<input type="text" name="nama" class="form_login" placeholder="Isi Nama Lengkap anda">
			</tr>
		</br>
		</br>
		<a class="selesai" href="daftarpoli.html">Login</a>
		</form>
	</div>

	<section class="footer">
		<div class="contact">
			<p>Hubungi Kami</p>
			<ol>
				<ul><img src="<?php echo base_url('asset/') ?>img/iconfinder_94_171453.png" style="width: 15px; height: 15px; color: white;" alt="">Jl.Rumah sakit no.86</ul>
				<ul><img src="<?php echo base_url('asset/') ?>img/iconfinder_phone_326545.png" style="width: 15px; height: 15px; color:white;" alt="">023-3444545</ul>
				<ul><img src="<?php echo base_url('asset/') ?>img/iconfinder_aiga_mail_134146.png" style="width: 15px; height: 15px;" alt="">rumahsakitcerdas@yahoo.com</ul>
				<ul><img src="<?php echo base_url('asset/') ?>img/iconfinder_globe_172473.png" style="width: 15px; height: 15px; color:white;" alt="">www.rumahsakitcerdas.com</ul>
			</ol>
		</div>
		<hr color="#DCDCDC;">
		<p>Copyright &copy;2019 Design By Nogi</p>
		</section>
 
    <script type="text/javascript">
        function validasi(){
            var nama = document.getElementById("nama").value;
			var nobpjs = document.getElementById("nobpjs").value;
			var diagnosa = document.getElementById("diagnosa").value;
        }
    </script>
</body>
</html>