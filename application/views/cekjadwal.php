<!DOCTYPE html>
<html>
    <head>
        <title>Pendaftaran</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/') ?>style.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Dosis&display=swap" rel="stylesheet">
    </head>
    <body>
        <div class="header" style="padding-top: 40px;">
            <h1 style="text-align: center;"><b>Selamat datang di situs nomor antrian rumah sakit cerdas</b></h1>
            <p style="text-align: center;">Web yang melayani pasien dengan pemberian nomor antrian yang cerdas,ramah dan efektif</p>

                <ul class="nav justify-content-center" style="padding-top: 27px;">
                    <li class="nav-item">
                        <a class="nav-link" style="color: black;" href="<?php echo base_url('Welcome')?>">Pendaftaran</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="<?php echo base_url('Welcome/cekjadwal')?>">Cek Jadwal</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: black;" href="#">Tata Cara</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="color: black;" href="<?php echo base_url('Welcome/about')?>">Tentang Kami</a>
                    </li>
                </ul>
        </div>

        <div id="Daftar_poli">
            <p class="daftar_poli" style="margin:0px 0px; margin-bottom: 20px;">Silahkan Pilih Poli Tujuan anda</p>
            <div class="input-group mb-3" style="width:500px; margin:0px 400px;">
                <form action="<?php echo base_url('Welcome/cekjadwal')?>" method="post">
                <!-- <div class="input-group-prepend">
                    <button class="btn btn-outline-secondary" type="button">Poli</button>
                </div> -->
                <select class="custom-select" id="inputGroupSelect03" aria-label="Example select with button addon" name="pilihan">
                        <option selected>Silahkan pilih...</option>
                        <option value="Anak">Anak</option>
                        <option value="Penyakit Dalam">Penyakit Dalam</option>
                        <option value="gigi">Gigi Spesialis </option>
                        <option value="obgyn">Kandungan dan Kebidanan(Obgyn)</option>
                        <option value="umum">Dokter Umum</option>
                        <option value="tht">Telinga Hidung Tenggorokan</option>
                        <option value="psikiatri">Psikiatri</option>
                        <option value="mata">mata</option>
                        <option value="radiologi">Radiologi</option>
                        <option value="saraf">Saraf</option>
                        <option value="Andrologi">Andrologi</option>
                        <option value="bedah">Bedah</option>
                        <option value="jantung">Jantung</option>
                    </select>
                    <input class="selanjutnya" type="submit" value="Selanjutnya" style="margin-left:350px; text-decoration: none; height:50px; width:150px; margin-top:10px;"></input>
                </form>
                <!-- <a class="selanjutnya" href="<?php echo base_url('Welcome/cekjadwal')?>" style="margin-left:400px; text-decoration: none;">Selanjutnya</a> -->
                        
            </div>
        </div>
        <section class="footer">
            <div class="contact">
                <p>Hubungi Kami</p>
                <ol>
                    <ul><img src="<?php echo base_url('asset/') ?>img/iconfinder_94_171453.png" style="width: 15px; height: 15px; color: white;" alt="">Jl.Rumah sakit no.86</ul>
                    <ul><img src="<?php echo base_url('asset/') ?>img/iconfinder_phone_326545.png" style="width: 15px; height: 15px; color:white;" alt="">023-3444545</ul>
                    <ul><img src="<?php echo base_url('asset/') ?>img/iconfinder_aiga_mail_134146.png" style="width: 15px; height: 15px;" alt="">rumahsakitcerdas@yahoo.com</ul>
                    <ul><img src="<?php echo base_url('asset/') ?>img/iconfinder_globe_172473.png" style="width: 15px; height: 15px; color:white;" alt="">www.rumahsakitcerdas.com</ul>
                </ol>
            </div>
        </section>
    </body>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</html>