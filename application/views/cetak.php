<!DOCTYPE html>
<html>
    <head>
        <title>Pendaftaran</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/') ?>style.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Dosis&display=swap" rel="stylesheet">
    </head>
    <body>
    <div class="header" style="padding-top: 40px;">
                    <h1 style="text-align: center;"><b>Selamat datang di situs nomor antrian rumah sakit cerdas</b></h1>
                    <p style="text-align: center;">Web yang melayani pasien dengan pemberian nomor antrian yang cerdas,ramah dan efektif</p>
                        <ul class="nav justify-content-center" style="padding-top: 27px;">
                            <li class="nav-item">
                                <a class="nav-link active" href="<?php echo base_url('Welcome')?>">Pendaftaran</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" style="color: black;" href="<?php echo base_url('Welcome/jadwal')?>">Cek Jadwal</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" style="color: black;" href="#">Tata Cara</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" style="color: black;" href="<?php echo base_url('Welcome/about')?>">Tentang Kami</a>
                            </li>
                        </ul>
        </div>
        <div class="container" style="margin-top: 60px;">
                    <div class="judul" style="text-align: center;">
                       <h6 style="margin-bottom: 20px;">Alur Pendaftaran</h6>
                        <img src="<?php echo base_url('asset/') ?>img/3.jadwal.png" alt="log" style="width: 250px; height: auto;">
                    </div> 
                </div>
        <h1 style="margin-top: 100px; text-align: center;">Registrasi Sukses</h1>
        <h3 style=" margin-top: 40px; text-align: center;">Anda terdaftar menjadi pasien poli anak pada hari senin, 01 Januari 2020 pada pukul 08.00 WIB</h3>\
        <h3 style="text-align: center;">atas nama Ahmad, dengan nomor rujukan 2211345</h3>
        <section class="footer" style="margin-top: 100px;">
            <div class="contact">
                <p>Hubungi Kami</p>
                <ol>
                    <ul><img src="img/iconfinder_94_171453.png" style="width: 15px; height: 15px; color: white;" alt="">Jl.Rumah sakit no.86</ul>
                    <ul><img src="img/iconfinder_phone_326545.png" style="width: 15px; height: 15px; color:white;" alt="">023-3444545</ul>
                    <ul><img src="img/iconfinder_aiga_mail_134146.png" style="width: 15px; height: 15px;" alt="">rumahsakitcerdas@yahoo.com</ul>
                    <ul><img src="img/iconfinder_globe_172473.png" style="width: 15px; height: 15px; color:white;" alt="">www.rumahsakitcerdas.com</ul>
                </ol>
            </div>
            <hr color="#DCDCDC;">
            <!-- <p>Copyright &copy;2019 Design By Nogi</p> -->
        </section>
    </body>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</html>