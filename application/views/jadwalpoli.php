<?php
$this->load->model('m_data');

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Pemilihan Jadwal</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/') ?>style.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Dosis&display=swap" rel="stylesheet">
    </head>
    <body>
    <div class="header" style="padding-top: 40px;">
                    <h1 style="text-align: center;"><b>Selamat datang di situs nomor antrian rumah sakit cerdas</b></h1>
                    <p style="text-align: center;">Web yang melayani pasien dengan pemberian nomor antrian yang cerdas,ramah dan efektif</p>
                        <ul class="nav justify-content-center" style="padding-top: 27px;">
                            <li class="nav-item">
                                <a class="nav-link active" href="<?php echo base_url('Welcome')?>">Pendaftaran</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" style="color: black;" href="<?php echo base_url('Welcome/cekjadwal')?>">Cek Jadwal</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" style="color: black;" href="#">Tata Cara</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" style="color: black;" href="<?php echo base_url('Welcome/about')?>">Tentang Kami</a>
                            </li>
                        </ul>
        </div>

        <!-- <div class="container" style="margin-top: 100px;">
            <div class="judul" style="text-align: center;">
                <h6 style="margin-bottom: 20px;">Alur Pendaftaran</h6>
                <img src="<?php echo base_url('asset/') ?>img/3.jadwal.png" alt="log" style="width: 250px; height: auto;">
            </div>
        </div>
        <div class="container" style="margin-top: 40px;"> -->

        <?php 
            foreach($result as $r)
            {
            
        ?>
        <p class="text-center" style="margin-top:80px;"><b>Jadwal Praktek Poli <?php $hasil = $this->m_data->get_data(array('id_poli' => $r->id_poli),'poli');
                                                            $hasil = $hasil->row();
                                                            echo $hasil->nama_poli;
                                                        ?></b></p>
        <p class="text-center"><b><?php echo $r->nama_dokter ?></b></p>
        
        
        
            <?php 
            }
            ?>
        

        <div class="row" id="Jadwal1">
            <div class="col-md-6">
                <p><b>Senin,01 Januari 2020</b></p>
                <table class="table1">
                    <tr>
                        <th>Pilih</th>
                        <th>Kategori Pasien</th>
                        <th>Jadwal</th>
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="spd0809"/></td> <!--Format Value = hari-poli-tgl -->
                        <td>Prioritas</td>
                        <td>Senin,08:00-09:00</td>
                        
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="spd0910"/></td>
                        <td>Prioritas</td>
                        <td>Senin,09:00-10:00</td>
                        
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="spd1011"/></td>
                        <td>Biasa</td>
                        <td>Senin,10:00-11:00</td>
                        
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="spd1112"/></td>
                        <td>Biasa</td>
                        <td>Senin,11:00-12:00</td>
                        
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="spd1314"/></td>
                        <td>Biasa</td>
                        <td>Senin,13:00-14:00</td>
                    
                    </tr>
                </table>
            </div>
            <div class="col-md-6">
                <p><b>Selasa,02 Januari 2020</b></p>
                <table class="table1">
                    <tr>
                        <th>Pilih</th>
                        <th>Kategori Pasien</th>
                        <th>Jadwal</th>
                    
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="sapd0809"/></td>
                        <td>Prioritas</td>
                        <td>Selasa,08:00-09:00</td>
                    
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="sapd0910"/></td>
                        <td>Prioritas</td>
                        <td>Selasa,09:00-10:00</td>
                    
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="sapd1011"/></td>
                        <td>Biasa</td>
                        <td>Selasa,10:00-11:00</td>
                    
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="sapd1112"/></td>
                        <td>Biasa</td>
                        <td>Selasa,11:00-12:00</td>
                    
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="sapd1314"/></td>
                        <td>Biasa</td>
                        <td>Selasa,13:00-14:00</td>
                    
                    </tr>
                </table>
            </div>
        </div>
        <div class="row" id="Jadwal2">
            <div class="col-md-6">
                <p><b>Rabu,03 Januari 2020</b></p>
                <table class="table1">
                    <tr>
                        <th>Pilih</th>
                        <th>Kategori Pasien</th>
                        <th>Jadwal</th>
                        
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="rpd0809"/></td>
                        <td>Prioritas</td>
                        <td>Rabu,08:00-09:00</td>
                        
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="rpd0910"/></td>
                        <td>Prioritas</td>
                        <td>Rabu,09:00-10:00</td>
                        
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="rpd1011"/></td>
                        <td>Biasa</td>
                        <td>Rabu,10:00-11:00</td>
                        
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="rpd1112"/></td>
                        <td>Biasa</td>
                        <td>Rabu,11:00-12:00</td>
                        
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="rpd1314"/></td>
                        <td>Biasa</td>
                        <td>Rabu,13:00-14:00</td>
                        
                    </tr>
                </table>
            </div>
            <div class="col-md-6">
                <p><b>Kamis,04 Januari 2020</b></p>
                <table class="table1">
                    <tr>
                        <th>Pilih</th>
                        <th>Kategori Pasien</th>
                        <th>Jadwal</th>
                        
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="kpd0809"/></td>
                        <td>Prioritas</td>
                        <td>Kamis,08:00-09:00</td>
                        
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="kpd0910"/></td>
                        <td>Prioritas</td>
                        <td>Kamis,09:00-10:00</td>
                        
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="kpd1011"/></td>
                        <td>Biasa</td>
                        <td>Kamis,10:00-11:00</td>
                        
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="kpd1112"/></td>
                        <td>Biasa</td>
                        <td>Kamis,11:00-12:00</td>
                        
                    </tr>
                    <tr>
                        <td><input type="radio" name="jadwal" value="kpd1314"/></td>
                        <td>Biasa</td>
                        <td>Kamis,13:00-14:00</td>
                        
                    </tr>
                </table>
            </div>



        </div>
        <section class="footer">
            <div class="contact">
                <p>Hubungi Kami</p>
                <ol>
                    <ul><img src="<?php echo base_url('asset/') ?>img/iconfinder_94_171453.png" style="width: 15px; height: 15px; color: white;" alt="">Jl.Rumah sakit no.86</ul>
                    <ul><img src="<?php echo base_url('asset/') ?>img/iconfinder_phone_326545.png" style="width: 15px; height: 15px; color:white;" alt="">023-3444545</ul>
                    <ul><img src="<?php echo base_url('asset/') ?>img/iconfinder_aiga_mail_134146.png" style="width: 15px; height: 15px;" alt="">rumahsakitcerdas@yahoo.com</ul>
                    <ul><img src="<?php echo base_url('asset/') ?>img/iconfinder_globe_172473.png" style="width: 15px; height: 15px; color:white;" alt="">www.rumahsakitcerdas.com</ul>
                </ol>
            </div>
            <hr color="#DCDCDC;">
            <p>Copyright &copy;2019 Design By Nogi</p>
        </section>

        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>
