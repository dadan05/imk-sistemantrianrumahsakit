<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
    {
        parent::__construct();
        $this->load->model('m_data');
        $this->load->helper('url');
    }

	public function index()
	{
		$this->load->view('main');
	}
	public function cekjadwal(){
		$pilihan = $this->input->post('pilihan');
		$poli_query = $this->m_data->get_data(array('nama_poli' => $pilihan),'poli');
		$poli = $poli_query->row();
		$jadwal_query = $this->m_data->get_data(array('id_poli' => $poli->id_poli),'jadwal');
		$jadwal['result'] = $jadwal_query->result();
		$this->load->view('jadwalpoli', $jadwal);
	}
	public function about(){
		$this->load->view('about');
	}
	public function login(){
		$this->load->view('daftarpoli');
	}
	public function jadwal(){
		$this->load->view('cekjadwal');
	}
	public function pilihjadwal(){
		$this->load->view('jadwalpolipd');
	}
	public function cetak(){
		$this->load->view('cetak');
	}
}
